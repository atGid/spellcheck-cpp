# SpellCheck Cpp

Gideon Ojo - CSC245 - April 6, 2018 - Digh

Project #3 - THE SPELL CHECKER

Classes
- BinarySearchTree
	This class is a BST that will be used to contain and print all the misspelled words
and their line numbers. Because it is a BST the words will be ordered alphabetically, and all
duplicates will be taken care of. To print the tree, an inorder traversal can be used. 
- LinkedList
	In this project we will be using a Seperate Chaining Hash Table so A linked list class
that can point to each record needs to be implemented.
- HashTable //
	Data structure that will be used to map values of any type item (in this program Strings)
- Dictionary //
	Contains a HashTable that is used to store a list of all the words in a 'dictionary'.
It can be quickly searched and added to.


