#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;
#include "dictionary.h"
#include "BinarySearchTree.h"

void printTitle();
void printOptions();
void fillDictionary(Dictionary <string> & dict);
void spellCheck(char * fileName[], const Dictionary <string> & dict, Dictionary <string> & add);
bool options(string word, int lineNum, Dictionary <string> & add, const Dictionary <string> & dict, Dictionary<string> & ignore);
void check(string word, const Dictionary <string> & dict);
//void notFound(const BinarySearchTree <BNode> & t);

struct BNode
{
        string word;
        int lineNum;
        bool operator < (BNode otherStruct) const
        {
                return ( word < otherStruct.word );
        }
};

void notFound(const BinarySearchTree <BNode> & t);

ostream& operator << (ostream& os, const BNode& bn)
{
	os << bn.word << " " << bn.lineNum;
	return os;
}

typedef BinarySearchTree<BNode> BST;

int main(int argc, char * argv [])
{
	if(argc < 2)
        {
                cout << "No file name given - - Aborting program" << endl;
                return 1;
        }
	Dictionary <string> myD("ZZZ", 1000); //Main dictionary object 
	Dictionary <string> adderD("ZZZ", 100); //For storing words to add to dictionary
	
	fillDictionary(myD);
	
	spellCheck(argv, myD, adderD);

	return 0;
}

void notFound(const BinarySearchTree <BNode> & t)
{
	ofstream outfile;
	outfile.open("Nfound.txt");	
	//outfile << t.findMax() << endl;
	//outfile << t.print();
	t.pTree();
}

void check(string word, const Dictionary <string> & dict)
{
	bool found = false;
	int j = 1;
	for(int i = 0; i < word.size(); i++)
	{
		char temp = word[j];
		word[j] = word[i];
		word[i] = temp;
		if(dict.lookup(word) != "ZZZ")
		{
			cout << endl << "\tSuggested spelling is = " << word << endl;
			found = true;
			i = word.size();
		}
		else
		{
			char temp = word[j];
			word[j] = word[i];
			word[i] = temp;
			j++;
		}
	}
	
	if(!found)
		cout << "\tNo suggested spellings found in the Dictionary" << endl;
	cout << endl << "\tPress Enter to continue... ";
	string t;
	getline(cin, t);
}

bool options(string word, int lineNum, Dictionary <string> & add, const Dictionary <string> & dict, Dictionary<string> & ignore)
{
	printTitle();
	cout << "\t" << word << " on line " << lineNum << " was not found." << endl;
	printOptions();

	cout << "\tSelection: ";
	string in;
	getline(cin, in);
	char in2 = tolower(in[0]);
	bool sw = true;

	ofstream outfile;
	outfile.open("dict.txt", ios::app);

	while(sw)
	{
		switch(in2)
		{
			case 'a':
				add.insert(word);
				outfile << word << endl;
				sw = false;
				break;
			case 'i':
				ignore.insert(word);
				sw = false;
				break;
			case 'g':
				sw = false;
				break;
			case 's':
				//
				check(word, dict);
				sw = false;
				break;
			case 'q':
				return false;
				sw = false;
				break;
			default:
				cout << "\tInvalid option. Try again." << endl;
				cout << "\tSelection: ";
				getline(cin, in);
				in2 = tolower(in[0]);
				break;
		}
	}
	outfile.close();
	return true;
}

void spellCheck(char * fileName[], const Dictionary <string> & dict, Dictionary <string> & add)
{
	ifstream infile(fileName[1]);
	string s;
	int lineCount = 0;
	bool status = true;

	BNode def = {"ZZZ", -1};
	BST myBST(def);

	Dictionary<string> ignore("ZZZ", 100);

	while(getline(infile, s) && (status == true))
	{	
		lineCount ++;
		int endS = 0;
		for(int i = 0; i < s.size(); i++)	//Take line
		{
			if(isalpha(s[i]))	//Find word
			{
				while(((s[endS] != ' ') && (endS < s.size())))
					endS++;

				if(!(isalpha(s[endS - 1])))
				{
					endS --;
				} 
				string x = s.substr(i, (endS - i ));
				//cout << x.size() << " " << x << endl;
				for(int j = 0; j < x.size(); j++)
					x[j] = tolower(x[j]);
				i = endS;
				endS++;
				
				if(ignore.lookup(x) != "ZZZ")
					;//Do nothing, hence ignore word
				else if((dict.lookup(x) == "ZZZ") && (add.lookup(x) == "ZZZ"))
				{
					BNode t = {x, lineCount};
					myBST.insert(t);

					status = options(x, lineCount, add, dict, ignore);
					if(status == false)
					{
						status = false;
						break;
					}
				}
				else
					;
			}
			else
				endS++;
		}
	}

	if(status)
		cout << endl << "\tThe spell checker is complete!" << endl << endl;
	else
		cout << endl << "\tNow Exiting..." << endl << endl;

	//myBST.printTree();
	notFound(myBST);
}

void fillDictionary(Dictionary <string> & dict)
{
	ifstream infile("dict.txt");
	string s = "";
	while(getline(infile, s))
	{
		dict.insert(s);
	}
}

void printTitle()
{
	system("clear");
	cout << endl << "\t~!@#$%^&*()  THE SPELL CHECKER PROGRAM!  )(*&^%$#@!~" << endl << endl;
}

void printOptions()
{
	cout << endl
	 << "\t(A) Add word to the dictionary." << endl
	 << "\t(I) Ignore future references." << endl
	 << "\t(G) Go to the next word." << endl
	 << "\t(S) Search for a spelling suggestion." << endl
	 << "\t(Q) Quit the Spell Checker." << endl << endl;
}
