#include <iostream>
#include <string>
#include "BinarySearchTree.h"
using namespace std;

struct BNode
{
        string word;
        int lineNum;
        bool operator < (BNode otherStruct) const
        {
                return ( word < otherStruct.word );
        }
        //
};
        
ostream& operator<< (ostream& os, const BNode& bn)
{
	os << bn.word << " " << bn.lineNum;
	return os;
}
        

typedef BinarySearchTree<string> BST;
typedef BinarySearchTree<BNode> BT;
int main()
{
	BinarySearchTree<int> myTree(-1);
	myTree.insert(10);
	myTree.insert(25);
	myTree.insert(5);
	myTree.insert(31);

	myTree.printTree();

	BST myBST("ZZZ");
	myBST.insert("Hello");
	myBST.insert("World");

	myBST.printTree();
	
	BNode fault{"ZZZ", -1};

	BNode bN1{"Word", 1};
	BNode bN2{"ANother", 5};
	BT myBT(fault);
	myBT.insert(bN1);
	myBT.insert(bN2);
	myBT.printTree();

	return 0;
}
